import logging
import json
import subprocess
from contextlib import redirect_stdout
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")

async def execute_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    raw_command = update.message.text.replace('/run ', '')
    command = raw_command.split(' ')
    result = subprocess.Popen(command, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process_id = f'Process id: {result.pid}\n'
    try:
        result.wait(timeout=5)
    except:
        output = f'Process timed out\n'
    finally:
        output = result.stdout.read()
    await context.bot.send_message(chat_id=update.effective_chat.id, text=f'{process_id}\n{output}')

if __name__ == '__main__':
    with open('config.json') as f:
        config = json.load(f)
        
    application = ApplicationBuilder().token(config["BotToken"]).build()
    
    start_handler = CommandHandler('start', start)

    execute_command_handler = CommandHandler('run', execute_command)

    application.add_handler(start_handler)

    application.add_handler(execute_command_handler)
    
    application.run_polling()